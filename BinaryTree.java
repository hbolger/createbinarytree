
public class binaryTree {
	
	Node root;
	
	public void addNode(int key, String name) {
		
		Node newNode = new Node(key, name);
		
		if(root == null)
			root = newNode;
		
		else {
			
			Node focusNode = root;
			Node parent;
			
			while(true) {
				
				parent = focusNode;
				if(key < focusNode.key) {
					
					focusNode = focusNode.leftChild;
					
					if(focusNode == null) {
						
						parent.leftChild = newNode;
						return;
						
					}
					
				}
				
				else {
					
					focusNode = focusNode.rightChild;
					
					if(focusNode == null) {
						
						parent.rightChild = newNode;
						return;
						
					}
					
				}
				
			}
			
		}
		
	}
	
	public void inOrderTraverseTree(Node focusNode) {
		
		if(focusNode != null) {
			
			inOrderTraverseTree(focusNode.leftChild);
			System.out.println(focusNode);
			inOrderTraverseTree(focusNode.rightChild);
			
		}
		
	}
	
	public void preOrderTraverseTree(Node focusNode) {
		
		if(focusNode != null) {
			
			System.out.println(focusNode);
			preOrderTraverseTree(focusNode.leftChild);
			preOrderTraverseTree(focusNode.rightChild);
			
		}
		
	}
	
	public void postOrderTraverseTree(Node focusNode) {
		
		if(focusNode != null) {
			
			postOrderTraverseTree(focusNode.leftChild);
			postOrderTraverseTree(focusNode.rightChild);
			System.out.println(focusNode);

		}
		
	}
	
	public Node findNode(int key) {
	
		if(root == null)
			return null;
		
		Node focusNode = root;
		
		while(focusNode.key != key) {
			
			if(key < focusNode.key)
				focusNode = focusNode.leftChild;
			
			else
				focusNode = focusNode.rightChild;
			
			if(focusNode == null)
				return null; 
			
		}
		
		return focusNode;
		
	}
	
	public boolean lowestCommonAncestor(Node root, Node lca, int key1, int key2) {
				
		if (root == null)
			return false;
		
		if (root.key == key1 || root.key == key2) {
			
			lca.key = root.key;
			lca.name = root.name;
			return true;
			
		}
		
		boolean left = lowestCommonAncestor(root.leftChild, lca, key1, key2);
		boolean right = lowestCommonAncestor(root.rightChild, lca, key1, key2);
		
		if (left && right) {
			lca.key = root.key;
			lca.name = root.name;
		}
			
		
		return left || right;
		
	}
	
	
	public int lcaInt(Node root, int key1, int key2) {
		
		Node lca = new Node(-1, "");
		
		lowestCommonAncestor(root, lca, key1, key2);
		if(lca.key != -1)
			return lca.key;
		
		else
			return -1;
		
	}
	
	
	public void lca(Node root, int key1, int key2) {
		
		Node lca = new Node(-1, "");
		
		
		//if (findNode(key1) != null && findNode(key2) != null)
		lowestCommonAncestor(root, lca, key1, key2);
		
		if (lca.key != -1)
			System.out.println("Lowest Common Ancestor is " + lca.key + " " + lca.name);
		
		else
			System.out.println("Lowest Common Ancestor Does Not Exist");
		
	}
	
	
	public boolean remove(int key) {
	
		if(root == null)
			return true;
		
		Node focusNode = root;
		Node parent = root;
		
		boolean isLeftChild = true;
		
		//find where it is and whether it is a right or left child
		while(focusNode.key != key) {
			
			parent = focusNode;
			
			if(key < focusNode.key) {
				
				isLeftChild = true;
				focusNode = focusNode.leftChild;
				
			}
			
			else {
				
				isLeftChild = false;
				focusNode = focusNode.rightChild;
				
			}
			
			if(focusNode == null)
				return false;
			
		}
		
		//delete leaf nodes
		if(focusNode.leftChild == null && focusNode.rightChild == null) {
			
			if(focusNode == root)
				root = null;
			
			else if(isLeftChild)
				parent.leftChild = null;
			
			else
				parent.rightChild = null;
			
		}
		
		//one child right child is null
		else if(focusNode.rightChild == null) {
			
			if(focusNode == root)
				root = focusNode.leftChild;
			
			else if(isLeftChild)
				parent.leftChild = focusNode.leftChild;
			
			else
				parent.rightChild = focusNode.rightChild;
			
		}
		
		//one child left child is null
		else if(focusNode.leftChild == null) {
			
			if(focusNode == root)
				root = focusNode.rightChild;
			
			else if(isLeftChild)
				parent.leftChild = focusNode.rightChild;
			
			else
				parent.rightChild = focusNode.leftChild;
			
		}
		
		//two children
		else {
			
			Node replacement = getReplacementNode(focusNode);
			
			if(focusNode == root)
				root = replacement;
			
			else if(isLeftChild)
				parent.leftChild = replacement;
			
			else
				parent.rightChild = replacement;
			
			replacement.leftChild = focusNode.leftChild;
			
		}
		
		return true;	
		
	}
	
	public Node getReplacementNode(Node replacedNode) {
		
		Node replacementParent = replacedNode;
		Node replacement = replacedNode;
		
		Node focusNode = replacedNode.rightChild;
		
		while(focusNode != null) {
			
			replacementParent = replacement;
			replacement = focusNode;
			focusNode = focusNode.leftChild;
			
		}
		
		if(replacement != replacedNode.rightChild) {
			
			replacementParent.leftChild = replacement.rightChild;
			replacement.rightChild = replacedNode.rightChild;
			
		}
		
		return replacement;
		
	}
	

	public static void main (String[] args) {
		
		binaryTree theTree = new binaryTree();
		
		theTree.addNode(50, "Boss");
		theTree.addNode(25, "VP");
		theTree.addNode(15, "Office Manager");
		theTree.addNode(2, "Intern");
		theTree.addNode(18, "Data");
		theTree.addNode(30, "Secretary");
		theTree.addNode(75, "Sales Manager");
		theTree.addNode(85, "Salesman");
		
		theTree.inOrderTraverseTree(theTree.root);
		System.out.println();
		theTree.preOrderTraverseTree(theTree.root);
		System.out.println();
		theTree.postOrderTraverseTree(theTree.root);
		
		System.out.println();
		System.out.println(theTree.findNode(30));

		System.out.println();
		System.out.println(theTree.remove(25));
		theTree.preOrderTraverseTree(theTree.root);
		
		theTree.lca(theTree.root, 2, 85);
		
	}

}



class Node{
	
	int key;
	String name;
	
	Node leftChild;
	Node rightChild;
	
	Node(int key, String name){
		
		this.key = key;
		this.name= name;
		
	}
	
	public String toString() {
		
		return name + " has a key " + key;
		
	}
	
}