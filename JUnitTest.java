import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestBinaryTree {
	
	
	@Test
	void emptyTree() {
		
		binaryTree theTree = new binaryTree();
		
		assertEquals(-1, theTree.lcaInt(theTree.root, 5, 10));
		
	}
	
	
	@Test
	void bothNodesDoNotExist() {
		
		binaryTree theTree = new binaryTree();

		theTree.addNode(50, "Boss");
		theTree.addNode(25, "VP");
		theTree.addNode(15, "Office Manager");
		theTree.addNode(2, "Intern");
		theTree.addNode(18, "Data");
		theTree.addNode(30, "Secretary");
		theTree.addNode(75, "Sales Manager");
		theTree.addNode(85, "Salesman");
		
		assertEquals(-1, theTree.lcaInt(theTree.root, 10, 20));
		
	}
	
	
	@Test
	void oneNodeExists() {
		
		binaryTree theTree = new binaryTree();

		theTree.addNode(50, "Boss");
		theTree.addNode(25, "VP");
		theTree.addNode(15, "Office Manager");
		theTree.addNode(2, "Intern");
		theTree.addNode(18, "Data");
		theTree.addNode(30, "Secretary");
		theTree.addNode(75, "Sales Manager");
		theTree.addNode(85, "Salesman");
		
		assertEquals(-1, theTree.lcaInt(theTree.root, 10, 25));
		
	}
	
	
	@Test
	void bothNodesSame() {
		
		binaryTree theTree = new binaryTree();

		theTree.addNode(50, "Boss");
		theTree.addNode(25, "VP");
		theTree.addNode(15, "Office Manager");
		theTree.addNode(2, "Intern");
		theTree.addNode(18, "Data");
		theTree.addNode(30, "Secretary");
		theTree.addNode(75, "Sales Manager");
		theTree.addNode(85, "Salesman");
		
		assertEquals(50, theTree.lcaInt(theTree.root, 50, 50));
		
	}
	
	
	@Test
	void baseCase() {
		
		binaryTree theTree = new binaryTree();
		
		theTree.addNode(50, "Boss");
		theTree.addNode(25, "VP");
		theTree.addNode(15, "Office Manager");
		theTree.addNode(2, "Intern");
		theTree.addNode(18, "Data");
		theTree.addNode(30, "Secretary");
		theTree.addNode(75, "Sales Manager");
		theTree.addNode(85, "Salesman");
		
		//base case
		assertEquals(25, theTree.lcaInt(theTree.root, 2, 30));
	
	}
	
	@Test
	void key1IsLCA() {

		
		binaryTree theTree = new binaryTree();
		
		theTree.addNode(50, "Boss");
		theTree.addNode(25, "VP");
		theTree.addNode(15, "Office Manager");
		theTree.addNode(2, "Intern");
		theTree.addNode(18, "Data");
		theTree.addNode(30, "Secretary");
		theTree.addNode(75, "Sales Manager");
		theTree.addNode(85, "Salesman");

		//lca is key1
		assertEquals(15, theTree.lcaInt(theTree.root, 2, 15));

	}
	
	
	@Test
	void rootAsLCA() {

		
		binaryTree theTree = new binaryTree();
		
		theTree.addNode(50, "Boss");
		theTree.addNode(25, "VP");
		theTree.addNode(15, "Office Manager");
		theTree.addNode(2, "Intern");
		theTree.addNode(18, "Data");
		theTree.addNode(30, "Secretary");
		theTree.addNode(75, "Sales Manager");
		theTree.addNode(85, "Salesman");

		assertEquals(50, theTree.lcaInt(theTree.root, 75, 18));

		
	}
	
		@Test
	public void testDelete() {

		binaryTree theTree = new binaryTree();

		theTree.remove(1);
		assertNull(theTree.findNode(1));

		theTree.addNode(50, "Boss");
		theTree.remove(50);
		assertEquals(null, theTree.root);

		theTree.addNode(50, "Boss");
		theTree.addNode(25, "VP");
		theTree.addNode(15, "Office Manager");
		theTree.addNode(2, "Intern");
		theTree.addNode(18, "Data");
		theTree.addNode(30, "Secretary");
		theTree.addNode(75, "Sales Manager");
		theTree.addNode(85, "Salesman");       



		//Remove Leaf
		theTree.remove(2);
		assertEquals(null, theTree.root.leftChild.leftChild.leftChild);

		//Single Child
		theTree.remove(75);
		assertEquals(85, theTree.root.rightChild.key);

		//Two Children
		theTree.remove(25);
		assertEquals(30, theTree.root.leftChild.key);

	}


	@Test
	public void testFind()
	{

		binaryTree theTree = new binaryTree();
		
		theTree.addNode(1, "A");
		theTree.addNode(3, "C");
		assertNull(theTree.findNode(2));
		
		assertNotNull(theTree.findNode(1));
		
	}
	
	
	@Test
	public void testAddNode()
	{
		binaryTree theTree = new binaryTree();
		
		theTree.addNode(50, "Boss");
		theTree.addNode(25, "VP");
		theTree.addNode(75, "Sales Manager");
		
		assertEquals(50, theTree.root.key);
		assertEquals("Boss", theTree.root.name);
		
		assertEquals(25, theTree.root.leftChild.key);
		assertEquals("VP", theTree.root.leftChild.name);
		
		assertEquals(75, theTree.root.rightChild.key);
		assertEquals("Sales Manager", theTree.root.rightChild.name);
		
	}
	
}
	
	